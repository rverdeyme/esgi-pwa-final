
const { GenerateSW } = require("workbox-webpack-plugin");

module.exports = {
  // publicPath: process.env.NODE_ENV === "development" ? "/vuejs-pwa/" : "",

  configureWebpack: {
    plugins: [new GenerateSW()]
  },

  pwa: {
    name: 'OpenEat',
    themeColor: '#273c75',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'white-translucent'
  }
};