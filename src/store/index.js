import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        listCommands: [],
        listFavs: [],
        connected: true
    },
    mutations: {
        toggleFav(state, item) {
            let index = this.state.listFavs.findIndex(element => {
                return element.id === item.id;
            });
            if (index >= 0) {
                this.state.listFavs.splice(index, 1);
            } else {
                this.state.listFavs.push(item);
            }
        },
        addListCommands(state, item) {
            let a = false;
            this.state.listCommands.forEach(element => {
                if(element.id == item.id) {
                    if (element.nbr) {
                        element.nbr++;
                    } else {
                        element.nbr = 2;
                    }
                    a = true;
                }
            });
            if(!a) {
                this.state.listCommands.push(item);
            }
        },
        setListCommands(state, newListCommands) {
            this.state.listCommands = newListCommands;
        },
        setConnected(state, newConnected) {
            this.state.connected = newConnected;
        },
    },
    getters: {
        getListCommands : state => state.listCommands,
        getListFavs : state => state.listFavs,
        getConnected : state => state.connected,
    }
})
