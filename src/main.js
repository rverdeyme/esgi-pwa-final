import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import * as firebase from "firebase";       
import VueFirebase from 'vue-firebase'
import '../node_modules/@fortawesome/fontawesome-free/css/all.css'
import '../node_modules/@fortawesome/fontawesome-free/js/all.js'
import Notifications from 'vue-notification'
import wb from "./registerServiceWorker";


// Config firebase project
const FBCONFIG = {
  apiKey: "AIzaSyBZhKHj-0m-PGdGRNogfP232NcQGC2mh9M",
  authDomain: "openeat-889bd.firebaseapp.com",
  databaseURL: "https://openeat-889bd.firebaseio.com",
  projectId: "openeat-889bd",
  storageBucket: "openeat-889bd.appspot.com",
  messagingSenderId: "703971128446",
  appId: "1:703971128446:web:2e78888602685d2a89542f",
  measurementId: "G-E5WNEWRKFB"
};

Vue.use(VueFirebase, {firebase: firebase, config: FBCONFIG});
Vue.use(Notifications);

Vue.prototype.$workbox = wb;
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
