import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Register from '../views/Register.vue'
import Login from '../views/Login.vue'
import Resto from '../views/Resto.vue'
import Basket from '../views/Basket.vue'
import Account from '../views/Account.vue'
import Favoris from '../views/Favoris.vue'
import Command from '../views/Command.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/signup',
    name: 'Register',
    component: Register
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  { 
    path: '/restaurant/:id', 
    name: 'Resto',
    component: Resto 
  },
  { 
    path: '/account', 
    name: 'Account',
    component: Account 
  },
  { 
    path: '/basket', 
    name: 'Basket',
    component: Basket 
  },
  { 
    path: '/favoris', 
    name: 'Favoris',
    component: Favoris 
  },
  { 
    path: '/command', 
    name: 'Command',
    component: Command 
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
